<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="./css/style.css" rel="stylesheet" type="text/css">
        <title>ユーザー編集</title>
    </head>
    <body>
    <h1>ユーザー編集</h1>
        <div class="main-contents">
           <div class="header">
           		<a href="./">ホーム</a>
           		<a href="usermanage">ユーザー管理</a>
	       		<a href="logout">ログアウト</a>
           </div>
        </div>
        <div class="profile">
			<div class="name">
				<h2>
					<c:out value="${loginUser.name}" />
				</h2>
			</div>
			<div class="account">
				@
				<c:out value="${loginUser.account}" />
			</div>
		</div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
	    	<form action="useredit" method="post">
	            <div class="useredit">
	            	<input name="id" value="${user.id}" id="id" type="hidden"/>

	                <label for="account">ログインID</label>
                	<input name="account" value="<c:out value="${user.account}"/>"/><br />

	                <label for="password">パスワード</label>
                	<input name="password" type="password" id="password"/><br />

	                <label for="password2">パスワード（確認）</label>
                	<input name="password2" type="password" id="password2"/><br />

	                <label for="name">名前</label>
                	<input name="name" value="<c:out value="${user.name}"/>"/><br />

					<c:if test="${loginUser.id != user.id }">
		                <label for="branchId">支店名</label>
		                <select name="branch_id">
		    				<c:forEach items="${branches}" var="branch">
		    					<c:if test="${ user.branchId == branch.id }">
			               			<option value="${branch.id }" selected="${ user.branchId}">
				               			<c:out value="${branch.name}" />
			               			</option>
		               			</c:if>
		               			<c:if test="${ user.branchId != branch.id }">
		               				<option value="${branch.id }">
			               				<c:out value="${branch.name}" />
		               				</option>
		               			</c:if>
							</c:forEach>
					 	</select><br />

		                <label for="position_id">部署・役職</label>
		          	 		<select name="position_id">
		    				<c:forEach items="${positions}" var="position">
		    					<c:if test="${ user.positionId == position.id }">
			               			<option value="${position.id }" selected="${ user.positionId}">
			               			<c:out value="${position.name}" />
			               		</c:if>
		               			<c:if test="${ user.positionId != position.id }">
		               				<option value="${position.id }">
			               				<c:out value="${position.name}" />
		               				</option>
		               			</c:if>
							</c:forEach>
					 	</select><br />
				 	</c:if>
						   <c:if test="${loginUser.id == user.id }">
						   		<input name="branch_id" value="${user.branchId}" id="branch_id" type="hidden"/>
						   		<input name="position_id" value="${user.positionId}" id="position_id" type="hidden"/>
					   	   </c:if>
	                 <br /><input type="submit" value="更新する" />
	                 </div>
	    		</form>
	    		<div class="copyright">Copyright(c)takeda</div>
</body>