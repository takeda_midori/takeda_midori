<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="./css/style.css" rel="stylesheet" type="text/css">
        <title>ログイン</title>
    </head>
    <body>

    	<div class="main-contents">
            <div class="login">
           	 	<c:if test="${ not empty errorMessages }">
	                <div class="errorMessages">
	                    <ul>
	                        <c:forEach items="${errorMessages}" var="messages">
	                            <li><c:out value="${messages}" />
	                        </c:forEach>
	                    </ul>
	                </div>
	                <c:remove var="errorMessages" scope="session"/>
            	</c:if>
	            <form action="login" method="post"><br />
	                <label for="account">ログインID</label>
	                <input name="account" value="<c:out value="${account}"/>"/> <br /><br />
	                <label for="password">パスワード</label>
	                <input name="password" type="password" id="password"/> <br /><br />
	                <input type="submit" value="ログイン" class="button"/> <br />
	            </form>
            </div>
         </div>
       <div class="copyright"> Copyright(c)takeda</div>
    </body>
</html>