<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>掲示板ホーム</title>

<script type="text/javascript">

			function check(){

				if(window.confirm('この投稿を削除してよろしいですか？')){ // 確認ダイアログを表示

					return true; // 「OK」時は送信を実行

				}
				else{ // 「キャンセル」時の処理

					return false; // 送信を中止

				}

			}


			function check2(){

				if(window.confirm('このコメントを削除してよろしいですか？')){ // 確認ダイアログを表示

					return true; // 「OK」時は送信を実行

				}
				else{ // 「キャンセル」時の処理

					return false; // 送信を中止

				}

			}
		</script>
</head>
<body>

	<div class="header">
		<h1>掲示板ホーム</h1>
			<div class="main-contents">
				<div class="header">
					<a href="newpost">新規投稿</a>
					<c:if test="${ loginUser.positionId == 1 }">
						<a href="usermanage">ユーザー管理</a>
					</c:if>
					<a href="logout">ログアウト</a>
				</div>
			</div>
	</div>

	<c:if test="${ not empty loginUser }">
			<div class="profile">
				<div class="name">
					<h2>
						<c:out value="${loginUser.name}" />
					</h2>
				</div>
				<div class="account">
					@
					<c:out value="${loginUser.account}" />
				</div>
			</div>
		</c:if>

		<div class="search-area">
			<h2>投稿の検索</h2>
			<form action="./" method="get">
				投稿日から検索：<input type="date" name="startAt" value="${ startAt}"></input>～
								<input type="date" name="endAt" value="${ endAt}"></input>
								<input type="submit" value="検索"></input><br>
				カテゴリから検索：<input type="text" name="category" value="${ category}"></input>
								  <input type="submit" value="検索"></input><br>
			</form>
		</div>


	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="messages">
						<li><c:out value="${messages}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
	</div>


	<div class="posts">
		<c:forEach items="${posts}" var="post">
			<div class="each-post">
				<div class="title">
					件名：<c:out value="${post.title}" />
				</div>
				<div class="post-text">
					<pre><c:out value="${post.text}" /></pre>
				</div>
				<div class="category">
					カテゴリ：
					<c:out value="${post.category}"/>
				</div>
				<div class="account-name">
				<span class="name"><c:out value="${post.name}" /></span>@<span
						class="account"><c:out value="${post.account}" /></span>
				</div>
				<div class="date">
					<fmt:formatDate value="${post.created_at}"
						pattern="yyyy/MM/dd HH:mm:ss" />
				</div>
				<c:if test="${ loginUser.id == post.userId }">
					<div class="post-delete">
						<form action="postdelete" method="post" onSubmit="return check()">
							<input type="hidden" name="id" value="${post.id}">
							<input type="submit" value="投稿削除">
						</form>
					</div>
				</c:if>
				</div>
				<div class="comments">
					<c:forEach items="${comments}" var="comment">
						<c:if test="${post.id == comment.postId }">
							<div class="comment">
								<div class="account-name">
									<span class="name"><c:out value="${comment.name}" /></span>@
									<span class="account"><c:out value="${comment.account}" /></span>
								</div>
								<div class="comment-text">
									<pre><c:out value="${comment.text}" /></pre>
								</div>
								<div class="date">
									<fmt:formatDate value="${comment.created_at}"
										pattern="yyyy/MM/dd HH:mm:ss" />
								</div>
								<div class="comment-delete">
								<c:if test="${loginUser.id == comment.userId}">
									<form action="commentdelete" method="post" onSubmit="return check2()">
											<input type="hidden" name="id" value="${comment.id}">
											<input type="submit" value="コメント削除">
									</form>
								</c:if>
								</div>
							</div>
						</c:if>
					</c:forEach>
				</div>
				<div class="form-area">
					<form action="comment" method="post">
						コメントする（500文字まで）<br />
						<c:if test="${ not empty errorMessages }">
							<div class="errorMessages">
								<ul>
									<c:forEach items="${errorMessages}" var="messages">
										<li><c:out value="${messages}" />
									</c:forEach>
								</ul>
							</div>
								<c:remove var="errorMessages" scope="session" />
						</c:if>
						<textarea name="text" cols="60" rows="5" class="comment-box"><c:if test="${post.id == comment.postId }"><c:out value="${comment.text}"/><c:remove var="comment" scope="session" /></c:if></textarea>
						<br /> <input type="hidden" name="post_id" value="${post.id}">
						<input type="submit" value="コメントする">

					</form>
				</div>
		</c:forEach>
	</div>
<div class="copyright">Copyright(c)takeda</div>
</body>
</html>