<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <link href="./css/style.css" rel="stylesheet" type="text/css">
        <title>ユーザー管理</title>

        <script type="text/javascript">
			function check(){

				if(window.confirm('ステータスを変更してよろしいですか？')){ // 確認ダイアログを表示

					return true; // 「OK」時は送信を実行

				}
				else{ // 「キャンセル」時の処理

					return false; // 送信を中止

				}

			}
		</script>
    </head>
	<body>
    <h1>ユーザー管理</h1>
        <div class="main-contents">
           <div class="header">
	        		<a href="./">ホーム</a>
	        		<a href="signup">ユーザー新規登録</a>
	        		<a href="logout">ログアウト</a>
           </div>
        </div>
        <div class="profile">
			<div class="name">
				<h2>
					<c:out value="${loginUser.name}" />
				</h2>
			</div>
			<div class="account">
				@
				<c:out value="${loginUser.account}" />
			</div>
		</div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
	<table border="1">
		<tr>
   			<th>ログインID</th><th>ユーザー名</th><th>支店</th><th>所属部署・役職</th><th>ステータス</th><th></th>
		</tr>
    	<c:forEach items="${users}" var="user">
   			<tr>
   				<td><c:out value="${user.account}" /></td>
   				<td><c:out value="${user.name}" /></td>
   				<td><c:out value="${user.branchName}" /></td>
   				<td><c:out value="${user.positionName}" /></td>
   				<td>
   					<c:if test="${ user.isWorking == 1 }">
                		<c:out value="稼働中" />
               		</c:if>
               		<c:if test="${ user.isWorking == 0 }">
                		<c:out value="停止中" />
               		</c:if>
               		<c:if test="${loginUser.id != user.id}">
		                <form action="usercontorol" method="post" onSubmit="return check()"><br />
		         			<input type="radio" name="isWorking" value="1" <c:if test="${ user.isWorking == 1}"> checked="checked"</c:if>/>復活
		         			<input type="radio" name="isWorking" value="0" <c:if test="${ user.isWorking == 0}"> checked="checked"</c:if>/>停止
		         			<input type="submit" name="isWorking" value="ステータス更新" />
		         			<input name="id" value="${user.id}" id="id" type="hidden"/>
		         		</form>
         			</c:if>
				</td>
				<td>
           		<input name="id" value="${user.id}" id="id" type="hidden"/>
                <a href="useredit?id=${user.id}" > 編集</a>
                </td>
    	</c:forEach>
	</table>
   <div class="copyright"> Copyright(c)takeda</div>
 	</body>
</html>