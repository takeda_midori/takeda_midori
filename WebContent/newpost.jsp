<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>新規投稿</title>
         <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
       <h1>新規投稿</h1>
        <div class="main-contents">
            <div class="header">
        			<a href="./">ホーム</a>
				    <a href="logout">ログアウト</a>
            </div>
            	<c:if test="${ not empty loginUser }">
				    <div class="profile">
				        <div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
				        <div class="account">
				            @<c:out value="${loginUser.account}" />
				        </div>
				    </div>
				</c:if>
			<c:if test="${ not empty errorMessages }">
               <div class="errorMessages">
                   <ul>
                       <c:forEach items="${errorMessages}" var="messages">
                           <li><c:out value="${messages}" />
                       </c:forEach>
                   </ul>
               </div>
               <c:remove var="errorMessages" scope="session"/>
           </c:if>
			<div class="post-form-area">
			        <form action="newpost" method="post">
			            件名（30文字以下）<br />
			           <input name="title" type="text" size="60" value="<c:out value="${post.title}"/>"/><br />
			            本文（1000文字以下）<br />
			            <textarea name="text" cols="100" rows="5" class="tweet-box"><c:out value="${post.text}"/></textarea><br />
			            カテゴリ（10文字以下）<br />
			            <input name="category" type="text" size="40" value="<c:out value="${post.category}"/>"/><br />
			            <input type="submit" value="投稿する">
			        </form>
			</div>
			<div class="post">
			    <c:forEach items="${posts}" var="post">
			            <div class="post">
			                <div class="account-name">
			                    <span class="account"><c:out value="${post.account}" /></span>
			                    <span class="name"><c:out value="${post.name}" /></span>
			                </div>
			                <div class="title"><c:out value="${post.title}" /></div>
			                <div class="text"><c:out value="${post.text}" /></div>
			                <div class="category"><c:out value="${post.category}" /></div>
			                <div class="date"><fmt:formatDate value="${post.created_at}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
			            </div>
			    </c:forEach>
			</div>
            <div class="copyright"> Copyright(c)takeda</div>
        </div>
    </body>
</html>