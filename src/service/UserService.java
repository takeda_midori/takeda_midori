package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import dao.UserDao;
import utils.CipherUtil;


//ユーザーの新規登録
public class UserService {
	public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);


            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	//すべてのユーザー情報の取得
	public List<User> getAllUser() {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        UserDao userDao = new UserDao();
	        List<User> ret = userDao.getAllUser(connection);

	        commit(connection);

	        return ret;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}

	//各ユーザー情報の取得
	public User getId(int id) {

	    Connection connection = null;
	    try {
	        connection = getConnection();

	        UserDao userDao = new UserDao();
	        User user = userDao.getId(connection, id);

	        commit(connection);

	        return user;

	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }

	}

	//ユーザー情報の更新
	public User update(User user) {

		Connection connection = null;
        try {
            connection = getConnection();

            System.out.println("暗号化前のパスワード:" + user.getPassword());

            if(user.getPassword().length() > 0) {
            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);
            System.out.println(encPassword);

            }

            UserDao userDao = new UserDao();
            userDao.update(connection, user);

            commit(connection);

            return user;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }


	}

	//アカウント停止・復活切り替え
	public User change(User user) {

		Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            userDao.change(connection, user);

            commit(connection);

            return user;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

	//アカウントの重複チェック
		public static User getAccount(String account) {



		    Connection connection = null;
		    try {
		        connection = getConnection();

		        UserDao userDao = new UserDao();

		        commit(connection);


		        return userDao.checkUser(connection, account);



		    } catch (RuntimeException e) {
		        rollback(connection);
		        throw e;
		    } catch (Error e) {
		        rollback(connection);
		        throw e;
		    } finally {
		        close(connection);
		    }
}
}
