package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import beans.Post;
import beans.UserPost;
import dao.PostDao;
import dao.UserPostDao;

//新規投稿
public class PostService {
	public void register(Post post) {

        Connection connection = null;
        try {
            connection = getConnection();

            PostDao postDao = new PostDao();
            PostDao.insert(connection, post);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
        }

//投稿表示
private static final int LIMIT_NUM = 1000;

public List<UserPost> getPost(String startAt, String endAt, String category) {

    Connection connection = null;
    try {
        connection = getConnection();

        String sdDate1 = "2018-01-01 00:00:00";
        String st = "";

        if (startAt == null || startAt.length() == 0) {
        	startAt = sdDate1;
        	System.out.println("startAtが空欄のとき：" + startAt);
        }

	     Date date2 = new Date();

	     SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");



      	if(endAt == null || endAt.length() == 0) {
      		endAt = sdFormat.format(date2);

      		System.out.println("endAtが空欄のとき：" + endAt);
      	} else {
      		endAt = endAt + " 23:59:59";
      	}

      	if(category == null || category.length() == 0) {
      		category = st;
      		System.out.println("categoryが空欄のとき");
      	}



      		 UserPostDao postDao = new UserPostDao();
             List<UserPost> ret = postDao.getUserPosts(connection, LIMIT_NUM, startAt, endAt, category);

      	commit(connection);
      	return ret;



    } catch (RuntimeException e) {
        rollback(connection);
        throw e;
    } catch (Error e) {
        rollback(connection);
        throw e;
    } finally {
        close(connection);
    }
}

//投稿の削除
public void delete(int id) {

	Connection connection = null;
    try {
        connection = getConnection();

        PostDao postDao = new PostDao();
        postDao.delete(connection, id);

        commit(connection);
    } catch (RuntimeException e) {
        rollback(connection);
        throw e;
    } catch (Error e) {
        rollback(connection);
        throw e;
    } finally {
        close(connection);
    }
}
}
