package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Post;
import beans.User;
import service.PostService;

@WebServlet(urlPatterns = { "/newpost" })
public class NewPostServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        User user = (User) session.getAttribute("loginUser");

        Post post = new Post();
        post.setTitle(request.getParameter("title"));
        post.setText(request.getParameter("text"));
        post.setCategory(request.getParameter("category"));
        post.setUserId(user.getId());

        if (isValid(request, messages) == true) {
            new PostService().register(post);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            request.setAttribute("post", post);
            request.getRequestDispatcher("newpost.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String title = request.getParameter("title"); //件名
        String text = request.getParameter("text"); //投稿内容
        String category = request.getParameter("category"); //カテゴリ

        System.out.println("件名の文字数は；" + title.length());
        System.out.println("新規投稿の文字数は；" + text.length());
        System.out.println("カテゴリの文字数は；" + category.length());

        if (StringUtils.isEmpty(title) || StringUtils.isBlank(title)) {
            messages.add("※件名を入力してください");
            System.out.println("件名が空欄");
        }
        if (StringUtils.isEmpty(text) || StringUtils.isBlank(text)) {
            messages.add("※本文を入力してください");
            System.out.println("投稿がが空欄");
        }
        if (StringUtils.isEmpty(category) == true || StringUtils.isBlank(category)) {
            messages.add("※カテゴリを入力してください");
            System.out.println("カテゴリが空欄");
        }

        if (30 < title.length()) {
        	System.out.println("件名が31文字以上");
            messages.add("※件名は30文字以下で入力してください");
        }
        if (1000 < text.length()) {
        	System.out.println("投稿が1001文字以上");
        	messages.add("※本文は1000文字以下で入力してください");

        }
        if (10 < category.length()) {
        	System.out.println("カテゴリが11文字以上");
        	messages.add("※カテゴリは10文字以下で入力してください");
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

        @Override
        protected void doGet(HttpServletRequest request,
                HttpServletResponse response) throws IOException, ServletException {

            User user = (User) request.getSession().getAttribute("loginUser");
            boolean isShowPostForm;
            if (user != null) {
                isShowPostForm = true;

            } else {
                isShowPostForm = false;
            }
            request.setAttribute("isShowPostForm", isShowPostForm);
            request.getRequestDispatcher("/newpost.jsp").forward(request, response);

        }
    }
