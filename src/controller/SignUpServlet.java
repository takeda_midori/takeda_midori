package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<beans.Branch> branches = new BranchService().getAllBranches();
		List<beans.Position> positions = new PositionService().getAllPositions();

		request.setAttribute("branches", branches);
		request.setAttribute("positions", positions);
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		List<beans.Branch> branches = new BranchService().getAllBranches();
		List<beans.Position> positions = new PositionService().getAllPositions();

		//UserService userService = new UserService();

		HttpSession session = request.getSession();

		User user = new User();
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
		user.setPositionId(Integer.parseInt(request.getParameter("position_id")));

		String account = request.getParameter("account");

		boolean loginIdValid = UserService.getAccount(account) == null;
//		エラー文追加
		if (loginIdValid == false) {
			messages.add("※このログインIDは既に使用されています");
		}

		if (isValid(request, messages) == true) {
				System.out.println("アカウント重複なし");

				new UserService().register(user);
				request.setAttribute("user", user);
				response.sendRedirect("usermanage");

			} else {


				session.setAttribute("errorMessages", messages);
				request.setAttribute("user", user);
				request.setAttribute("branches", branches);
				request.setAttribute("positions", positions);
				request.getRequestDispatcher("signup.jsp").forward(request, response);
			}
		}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		int branchId = Integer.parseInt(request.getParameter("branch_id"));
		int positionId = Integer.parseInt(request.getParameter("position_id"));

		System.out.println("パスワード：" + password);
		System.out.println("パスワード2：" + password2);


		if ((StringUtils.isEmpty(account) || StringUtils.isBlank(account))
				|| (account.length() < 6 || 20 < account.length())
						|| (!account.matches("^[a-zA-Z0-9]+$"))) {

			messages.add("※ログインIDは6文字以上20文字以内の半角英数字で入力してください");
		}

		if ((StringUtils.isEmpty(password) || StringUtils.isBlank(password))
				||  (password.length() < 6 || 20 < password.length())
						||  (!password.matches("^[a-zA-Z0-9]+$"))) {

			messages.add("※パスワードは6文字以上20文字以内の半角英数字で入力してください");

			} else if (!password.equals(password2)) {

				messages.add("※入力したパスワードと確認用パスワードが一致しません");
			}


		if (StringUtils.isEmpty(name) || StringUtils.isBlank(name)
				|| 11 < name.length()) {
			messages.add("※ユーザー名は10文字以下で入力してください");
		}

		if (!(branchId ==1 && positionId <= 2) && !(branchId > 1 && positionId >= 3)) {
			messages.add("※支店名と所属・部署の組み合わせが不正です");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
