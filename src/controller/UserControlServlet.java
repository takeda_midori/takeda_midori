package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = {"/usercontorol"})
public class UserControlServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

		@Override
	    protected void doGet(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

	        request.getRequestDispatcher("login.jsp").forward(request, response);
	    }

	    @Override
	    protected void doPost(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

	    	//List<String> messages = new ArrayList<String>();

	        HttpSession session = request.getSession();

	        int id = Integer.parseInt(request.getParameter("id"));
	        int isWorking = Integer.parseInt(request.getParameter("isWorking"));

	        User contorolUser = new User();
	        contorolUser.setId(Integer.parseInt(request.getParameter("id")));
	        contorolUser.setIsWorking(Integer.parseInt(request.getParameter("isWorking")));

	        User user = new UserService().change(contorolUser);

	        System.out.println("id：" + id);
	        System.out.println("isWorking：" + isWorking);

	        request.setAttribute("user", user);

	        System.out.println(user.getId());
	        System.out.println(user.getIsWorking());

	        response.sendRedirect("usermanage");
}
}