package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();
        session.removeAttribute("comment");

        //エラーメッセージの作成
        List<String> messages = new ArrayList<String>();

        User user = (User) session.getAttribute("loginUser");

        Comment comment = new Comment();
        comment.setPostId(Integer.parseInt(request.getParameter("post_id")));
        comment.setText(request.getParameter("text"));
        comment.setUserId(user.getId());


        if (isValid(request, messages) == true) {

            new CommentService().register(comment);

            response.sendRedirect("./");

        } else {

            session.setAttribute("errorMessages", messages);
            session.setAttribute("comment", comment);
            response.sendRedirect("./");


        }
    }

      private boolean isValid(HttpServletRequest request, List<String> messages) {

        String text = request.getParameter("text");

        System.out.println("textは：" + text);
        System.out.println("textの文字数は：" + text.length());

        if (StringUtils.isEmpty(text) || StringUtils.isBlank(text)) {
        	System.out.println("コメントが空欄");
            messages.add("※コメントを入力してください");
        }
        if (500 < text.length()) {
        	System.out.println("コメントが500文字以上");
            messages.add("※コメントは500文字以下で入力してください");
        }
        if (messages.size() == 0) {
            System.out.println("500文字以内");
            return true;

        } else {
            return false;
        }
    }
}