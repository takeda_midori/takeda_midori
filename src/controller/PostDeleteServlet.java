package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.PostService;

@WebServlet(urlPatterns = { "/postdelete" })
public class PostDeleteServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	 @Override
	    protected void doPost(HttpServletRequest request,
	            HttpServletResponse response) throws ServletException, IOException {

		 //HttpSession session = request.getSession();

		 int id = Integer.parseInt(request.getParameter("id"));

		 //post.setId(Integer.parseInt(request.getParameter("id")));
		 //request.setAttribute("posts", posts);

		 new PostService().delete(id);

		 response.sendRedirect("./");
	}

}
