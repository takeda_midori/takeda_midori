package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/useredit" })
public class UserEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	//該当するユーザー情報の取得
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		String stId = request.getParameter("id");

		if (stId == null || stId.length() == 0 || !stId.matches("^[0-9]+$")) {
			System.out.println("idが数値以外");
			messages.add("※不正な値が入力されました");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("usermanage");

		} else { //数値でidが存在しないとき

			int id = Integer.parseInt(stId);
			User user = new UserService().getId(id);
			System.out.println("編集id：" + id);

			if (user == null) {
				System.out.println("idが存在しないとき");
				messages.add("※指定した値は存在しません");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("usermanage");

			} else { //idが存在していたとき

					System.out.println("idが存在");

					List<beans.Branch> branches = new BranchService().getAllBranches();
					List<beans.Position> positions = new PositionService().getAllPositions();

					request.setAttribute("branches", branches);
					request.setAttribute("positions", positions);
					request.setAttribute("user", user);
					request.getRequestDispatcher("useredit.jsp").forward(request, response);

			}
		}
	}

	//バリデーションのエラーメッセージを表示
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);

		int id = Integer.parseInt(request.getParameter("id"));
		String account = request.getParameter("account");

		User checkUser = UserService.getAccount(account);//存在したaccountをcheckUserに格納

		boolean loginIdValid = (checkUser == null || id == checkUser.getId());
		if (loginIdValid == false) {
			System.out.println("loginIdValid==false");
			messages.add("※このログインIDは既に使用されています");
		}

		if (isValid(request, messages) == true && loginIdValid == true) {
			System.out.println("エラーなし");

			new UserService().update(editUser);

			request.setAttribute("user", editUser);
			response.sendRedirect("usermanage");

		} else {
			System.out.println("エラーあり");

			session.setAttribute("errorMessages", messages);
			request.setAttribute("user", editUser);
			List<beans.Branch> branches = new BranchService().getAllBranches();
			List<beans.Position> positions = new PositionService().getAllPositions();

			request.setAttribute("branches", branches);
			request.setAttribute("positions", positions);

			request.getRequestDispatcher("useredit.jsp").forward(request, response);
		}
	}

	//ユーザーの更新
	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User edituser = new User();
		edituser.setId(Integer.parseInt(request.getParameter("id")));
		edituser.setAccount(request.getParameter("account"));
		edituser.setName(request.getParameter("name"));
		edituser.setPassword(request.getParameter("password"));
		System.out.println("入力されたパスワード:" + request.getParameter("password"));
		System.out.println("パスワード空欄チェック：" + StringUtils.isEmpty(request.getParameter("password")));

		edituser.setBranchId(Integer.parseInt(request.getParameter("branch_id")));
		edituser.setPositionId(Integer.parseInt(request.getParameter("position_id")));
		return edituser;

	}

	//パスワードのバリデーション
	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		int branchId = Integer.parseInt(request.getParameter("branch_id"));
		int positionId = Integer.parseInt(request.getParameter("position_id"));


		if ((StringUtils.isEmpty(account) || StringUtils.isBlank(account))
				|| (account.length() < 6 || 20 < account.length())
				|| (!account.matches("^[a-zA-Z0-9]+$"))) {

			messages.add("※ログインIDは6文字以上20文字以内の半角英数字で入力してください");
		}

		if ((!StringUtils.isEmpty(password) || !StringUtils.isBlank(password))
				&& ((password.length() < 6 || 20 < password.length())
					|| (!password.matches("^[a-zA-Z0-9]+$")))) {

			messages.add("※パスワードは6文字以上20文字以内の半角英数字で入力してください");

		} else if (!password.equals(password2)) {

			messages.add("※入力したパスワードと確認用パスワードが一致しません");
		}

		if (StringUtils.isEmpty(name) || StringUtils.isBlank(name)
				|| 11 < name.length()) {
			messages.add("※ユーザー名は10文字以下で入力してください");
		}

		if (!(branchId == 1 && positionId <= 2) && !(branchId > 1 && positionId >= 3)) {
			messages.add("※支店名と所属・部署の組み合わせが正しくありません。");

		}



		if (messages.size() == 0) {
			return true; //パスワードを含めた更新処理へ
		} else {
			return false;
		}
	}
}