package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import beans.UserComment;
import beans.UserPost;
import service.CommentService;
import service.PostService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	 @Override
	    protected void doGet(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

		 request.setCharacterEncoding("UTF-8");//文字エンコード

	        User user = (User) request.getSession().getAttribute("loginUser");

	        String startAt = request.getParameter("startAt");
	        String endAt = request.getParameter("endAt");
	        String category = request.getParameter("category");

	        List<UserPost> posts = new PostService().getPost(startAt, endAt, category);
	        List<UserComment> comments = new CommentService().getComment();

	        //確認用
//	        System.out.println("startAt：" + startAt);
//	        System.out.println("endAt：" + endAt);
	        //System.out.println("category：" + ((UserPost) posts).getCategory());



	        request.setAttribute("posts", posts);
	        request.setAttribute("comments", comments);
	        request.setAttribute("startAt", startAt);
	        request.setAttribute("endAt", endAt);
	        request.setAttribute("category", category);

	        request.getRequestDispatcher("/top.jsp").forward(request, response);
	 }
}
