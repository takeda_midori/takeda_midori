package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserPost;
import exception.SQLRuntimeException;

public class UserPostDao {

	//usertableとpotst_tableの結合
	 public List<UserPost> getUserPosts(Connection connection,
			 int num, String startAt, String endAt, String category) {

		 PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("SELECT ");
	            sql.append("posts.id as id, ");
	            sql.append("posts.title as title, ");
	            sql.append("posts.text as text, ");
	            sql.append("posts.category as category, ");
	            sql.append("posts.user_id as user_id, ");
	            sql.append("users.account as account, ");
	            sql.append("users.name as name, ");
	            sql.append("posts.created_at as created_at ");
	            sql.append("FROM posts ");
	            sql.append("INNER JOIN users ");
	            sql.append("ON posts.user_id = users.id ");
	            sql.append("WHERE (posts.created_at ");
	            sql.append("BETWEEN ? ");
	            sql.append("AND ?) ");
	            sql.append("AND ");
	            sql.append("(category LIKE ?) ");
	            sql.append("ORDER BY created_at DESC limit " + num);

	            ps = connection.prepareStatement(sql.toString());

	            ps.setString(1, startAt);
				ps.setString(2, endAt);
				ps.setString(3, "%" + category + "%");

	            System.out.println(ps.toString());

	            ResultSet rs = ps.executeQuery();
	            List<UserPost> ret = toUserPostList(rs);

	            return ret;

	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	    }

	    private List<UserPost> toUserPostList(ResultSet rs)
	            throws SQLException {

	        List<UserPost> ret = new ArrayList<UserPost>();
	        try {
	            while (rs.next()) {
	                String account = rs.getString("account");
	                String name = rs.getString("name");
	                int id = rs.getInt("id");
	                int userId = rs.getInt("user_id");
	                String title = rs.getString("title");
	                String text = rs.getString("text");
	                String category = rs.getString("category");
	                Timestamp created_at = rs.getTimestamp("created_at");

	                UserPost post = new UserPost();
	                post.setAccount(account);
	                post.setName(name);
	                post.setId(id);
	                post.setUserId(userId);
	                post.setTitle(title);
	                post.setText(text);
	                post.setCategory(category);
	                post.setCreated_at(created_at);

	                ret.add(post);
	            }
	            return ret;
	        } finally {
	            close(rs);
	        }
	    }
}
