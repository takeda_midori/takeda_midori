package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

public class UserCommentDao {

	public List<UserComment> getUserComments(Connection connection, int num) {

		PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.text as text, ");
            sql.append("comments.user_id as user_id, ");
            sql.append("comments.post_id as post_id, ");
            sql.append("users.account as account, ");
            sql.append("users.name as name, ");
            sql.append("comments.created_at as created_at ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("ORDER BY created_at ASC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserCommentList(rs);
            return ret;

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComment> toUserCommentList(ResultSet rs)
            throws SQLException {

        List<UserComment> ret = new ArrayList<UserComment>();
        try {
            while (rs.next()) {
                String account = rs.getString("account");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                int postId = rs.getInt("post_id");
                String text = rs.getString("text");
                Timestamp created_at = rs.getTimestamp("created_at");

                UserComment comment = new UserComment();
                comment.setAccount(account);
                comment.setName(name);
                comment.setId(id);
                comment.setUserId(userId);
                comment.setPostId(postId);
                comment.setText(text);
                comment.setCreated_at(created_at);

                ret.add(comment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}
