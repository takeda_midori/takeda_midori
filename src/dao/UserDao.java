package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.SQLRuntimeException;

public class UserDao {

	//ユーザーの新規登録
	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("account");
			sql.append(", password");
			sql.append(", name");
			sql.append(", branch_id");
			sql.append(", position_id");
			sql.append(", is_working");
			sql.append(", created_at");
			sql.append(", updated_at");
			sql.append(") VALUES (");
			sql.append("?"); // account
			sql.append(", ?"); // password
			sql.append(", ?"); // name
			sql.append(", ?"); // branch_id
			sql.append(", ?"); // positon_id
			sql.append(", 1"); //is_working
			sql.append(", CURRENT_TIMESTAMP"); // created_at
			sql.append(", CURRENT_TIMESTAMP"); // updated_at
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranchId());
			ps.setInt(5, user.getPositionId());

			System.out.println(ps.toString());
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//ログイン情報の取得
	public User getUser(Connection connection, String account, String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE (account = ?) AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, account);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();

			List<User> userList = toUserList(rs);

			if (userList.isEmpty() == true) {
				System.out.println("ログインユーザーが存在しないとき");
				return null;

			} else if (2 <= userList.size()) {
				System.out.println("ログイン同じユーザーが2つ以上存在したとき");
				throw new IllegalStateException("2 <= userList.size()");

			} else {
				System.out.println("ログインユーザーが1つ存在していたとき：" + userList.get(0));
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String account = rs.getString("account");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branchId = rs.getInt("branch_id");
				int positionId = rs.getInt("position_id");
				Timestamp createdAt = rs.getTimestamp("created_at");
				Timestamp updatedAt = rs.getTimestamp("updated_at");
				int isWorking = rs.getInt("is_working");

				User user = new User();
				user.setId(id);
				user.setAccount(account);
				user.setPassword(password);
				user.setName(name);
				user.setBranchId(branchId);
				user.setPositionId(positionId);
				user.setCreatedAt(createdAt);
				user.setUpdatedAt(updatedAt);
				user.setIsWorking(isWorking);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	//すべてのユーザー情報の取得
	public List<User> getAllUser(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id as id, ");
			sql.append("users.account as account, ");
			sql.append("users.password as password, ");
            sql.append("users.name as name, ");
			sql.append("users.branch_id as branch_id ,");
			sql.append("users.position_id as position_id, ");
			sql.append("positions.name as position_name, ");
			sql.append("users.is_working as is_working, ");
			sql.append("branches.name as branch_name, ");
			sql.append("users.created_at as created_at, ");
			sql.append("users.updated_at as updated_at ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN positions ");
			sql.append("ON users.position_id = positions.id ");
			sql.append("ORDER BY created_at ASC");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<User> ret = userList(rs);
			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> userList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String account = rs.getString("account");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branchId = rs.getInt("branch_id");
				String branchName = rs.getString("branch_name");
				int positionId = rs.getInt("position_id");
				String positionName = rs.getString("position_name");
				Timestamp createdAt = rs.getTimestamp("created_at");
				Timestamp updatedAt = rs.getTimestamp("updated_at");
				int isWorking = rs.getInt("is_working");

				User user = new User();
				user.setId(id);
				user.setAccount(account);
				user.setPassword(password);
				user.setName(name);
				user.setBranchId(branchId);
				user.setBranchName(branchName);
				user.setPositionId(positionId);
				user.setPositionName(positionName);
				user.setCreatedAt(createdAt);
				user.setUpdatedAt(updatedAt);
				user.setIsWorking(isWorking);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}


	//編集用ユーザー情報の取得
	public User getId(Connection connection, int id){

		System.out.println("UserDao ID:" + id );

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> ret = toUserList(rs);

			 if (ret.isEmpty() == true) {
		            return null;
		        } else if (2 <= ret.size()) {
		            throw new IllegalStateException("2 <= userList.size()");
		        } else {
		            return ret.get(0);
		        }

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//ユーザー情報の更新
	public void update(Connection connection, User user) {

		List<User> ret = new ArrayList<User>();
		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  account = ?");
			sql.append(", name = ?");

			if(StringUtils.isEmpty(user.getPassword())) {
				sql.append(", branch_id = ?");
				sql.append(", position_id = ?");
				sql.append(", updated_at = CURRENT_TIMESTAMP");
				sql.append(" WHERE");
				sql.append(" id = ?");

				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, user.getAccount());
				ps.setString(2, user.getName());
				ps.setInt(3, user.getBranchId());
				ps.setInt(4, user.getPositionId());
				ps.setInt(5, user.getId());

				System.out.println("パスワード入力なし" + ps.toString());

			} else {

				sql.append(", password = ?");
				sql.append(", branch_id = ?");
				sql.append(", position_id = ?");
				sql.append(", updated_at = CURRENT_TIMESTAMP");
				sql.append(" WHERE");
				sql.append(" id = ?");

				ps = connection.prepareStatement(sql.toString());

				ps.setString(1, user.getAccount());
				ps.setString(2, user.getName());
				ps.setString(3, user.getPassword());
				ps.setInt(4, user.getBranchId());
				ps.setInt(5, user.getPositionId());
				ps.setInt(6, user.getId());

				System.out.println("パスワード入力あり" + ps.toString());

			}

			ps.executeUpdate();

			ret.add(user);

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//ユーザー編集のアカウントチェック
	public User getEditAccount(Connection connection, String account) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE account = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, account);

			ResultSet rs = ps.executeQuery();

			List<User> userList = toUserList(rs);

			if (userList.isEmpty() == true) {
				System.out.println("ユーザー編集ユーザーが存在しないとき");
				return null;

			} else if (2 <= userList.size()) {
				System.out.println("ユーザー編集同じユーザーが2つ以上存在したとき");
				throw new IllegalStateException("2 <= userList.size()");

			} else {
				System.out.println("ユーザー編集ユーザーが1つ存在していたとき：" + userList.get(0));
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}



	//アカウントの停止・復活
	public void change(Connection connection, User user) {

		List<User> ret = new ArrayList<User>();
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("is_working = ?");
			sql.append(", updated_at = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, user.getIsWorking());
			ps.setInt(2, user.getId());

			System.out.println(ps.toString());

			ps.executeUpdate();

			ret.add(user);

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	//ユーザーの重複チェック
		public User checkUser(Connection connection, String account) {

			PreparedStatement ps = null;
			try {
				String sql = "SELECT * FROM users WHERE account = ?";

				ps = connection.prepareStatement(sql);
				ps.setString(1, account);

				System.out.println(ps.toString());

				ResultSet rs = ps.executeQuery();

				List<User> userList = toUserList(rs);

				if (userList.isEmpty() == true) {
					System.out.println("重複チェックユーザーが存在しないとき");
					return null;

				} else {
					System.out.println("重複チェックユーザーが存在するとき");
				  return userList.get(0);
				}

			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}
		}
}
