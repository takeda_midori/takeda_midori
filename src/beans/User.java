package beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable{
	private static final long serialVersionUID = 1L;

    private int id;
    private String account;
    private String password;
    private String name;
    private int branchId;
    private String branchName;
    private int positionId;
    private String positionName;
    private Date createdAt;
    private Date updatedAt;
    private int isWorking;

    public int getId() {
    	return id;
    }

	public String getAccount() {
		return account;
	}

	public String getPassword() {
		return password;
	}

	public String getName() {
		return name;
	}

	public int getBranchId() {
		return branchId;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public int getPositionId() {
		return positionId;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public int getIsWorking() {
		return isWorking;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}

	public void setPositionId(int positionId) {
		this.positionId = positionId;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public void setIsWorking(int isWorking) {
		this.isWorking = isWorking;
	}
}
