package beans;

import java.io.Serializable;
import java.util.Date;

public class Position implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
    private String name;
    private Date createdAt;

    public int getId() {
    	return id;
    }

	public String getName() {
		return name;
	}

	public Date getCreatedAt() {
		return createdAt;
	}


	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
}
