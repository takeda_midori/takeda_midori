package beans;

import java.io.Serializable;
import java.util.Date;

public class UserPost implements Serializable{
	private static final long serialVersionUID = 1L;

	private int id;
    private String account;
    private String name;
    private int userId;
    private String title;
    private String text;
    public Date getStartAt() {
		return startAt;
	}

	public void setStartAt(Date startAt) {
		this.startAt = startAt;
	}

	public Date getEndAt() {
		return endAt;
	}

	public void setEndAt(Date endAt) {
		this.endAt = endAt;
	}

	private String category;
    private Date startAt;
    private Date endAt;
    private Date created_at;

    public int getId() {
    	return id;
	}

	public String getAccount() {
		return account;
	}

	public String getName() {
		return name;
	}

	public int getUserId() {
		return userId;
	}

	public String getTitle() {
		return title;
	}

	public String getText() {
		return text;
	}

	public String getCategory() {
		return category;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setText(String text) {
		this.text= text;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

}
