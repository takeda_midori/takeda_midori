package beans;

import java.util.Date;

public class UserComment {
	private static final long serialVersionUID = 1L;

	private int id;
    private String account;
    private String name;
    private int userId;
    private int postId;
    private String text;
    private Date created_at;

    public int getId() {
    	return id;
	}

	public String getAccount() {
		return account;
	}

	public String getName() {
		return name;
	}

	public int getUserId() {
		return userId;
	}

	public int getPostId() {
		return postId;
	}

	public String getText() {
		return text;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setPostId(int postId) {
		this.postId = postId;
	}

	public void setText(String text) {
		this.text= text;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

}
