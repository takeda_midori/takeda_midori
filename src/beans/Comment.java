package beans;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
    private int userId;
    private int postId;
    private String text;
    private Date createdAt;
    private Date updatedAt;

    public int getId() {
    	return id;
	}

	public int getUserId() {
		return userId;
	}

	public int getPostId() {
		return postId;
	}

	public String getText() {
		return text;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setPostId(int postId) {
		this.postId = postId;
	}

	public void setText(String text) {
		this.text= text;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdated_at(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

}