package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter({"/*"})
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();
		String sp = ((HttpServletRequest) request).getServletPath();
		System.out.println("sp：" + sp);

		//ログインユーザーの取得
		User user = (User) session.getAttribute("loginUser");
		System.out.println("ログインユーザー：" + user);


		if( user == null && !sp.equals("/login") && !sp.equals("/css/style.css"))  {

			System.out.println("ログインしてください。");

			List<String> messages = new ArrayList<String>();
			messages.add("※ログインしてください");
			session.setAttribute("errorMessages", messages);
			((HttpServletResponse) response).sendRedirect("login");

		} else if ((sp.equals("/usermanage") || sp.equals("/useredit") || sp.equals("/signup"))
				&& user.getPositionId() > 1) {

			List<String> messages = new ArrayList<String>();
			messages.add("※権限がありません");
			session.setAttribute("errorMessages", messages);
			((HttpServletResponse) response).sendRedirect("./");

		} else {
			chain.doFilter(request, response);

		}
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
	}


	@Override
	public void destroy() {
	}
}